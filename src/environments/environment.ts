// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDKO7KHfmNPXSdnPNE1S8O6WdfEQbOwlsU",
    authDomain: "dream-prelo-shop.firebaseapp.com",
    databaseURL: "https://dream-prelo-shop.firebaseio.com",
    projectId: "dream-prelo-shop",
    storageBucket: "dream-prelo-shop.appspot.com",
    messagingSenderId: "395147362499"
  }
};
