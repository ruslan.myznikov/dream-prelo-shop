/**
 * User model
 */
export class User {

    /**
     * User first name
     * @type {string}
     */
    public name: string;

    /**
     * User e-mail
     * @type {string}
     */
    public email: string;

    /**
     * Url of user picture
     * @type {string}
     */
    public pictureUrl: string

    /**
     * @ignore
     */
    constructor(name: string, email: string, pictureUrl: string) {
        this.name = name;
        this.email = email;
        this.pictureUrl = pictureUrl
    }
}