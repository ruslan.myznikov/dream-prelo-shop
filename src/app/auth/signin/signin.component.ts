import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import { User } from '../models/user';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService) { }

  user: User;

  ngOnInit() {
  }

  doGoogleLogin() {
    this.authService.doGoogleLogin().then((data: any) => {
      let responsUserProfile = data.additionalUserInfo.profile;
      this.user = new User(responsUserProfile.given_name, responsUserProfile.email, responsUserProfile.picture);
      console.log(data.additionalUserInfo.profile);
      if(this.user) {
        // this.authService.userData.next(this.user);
        localStorage.setItem('name', this.user.name);
        localStorage.setItem('email', this.user.email);
        localStorage.setItem('pictureUrl', this.user.pictureUrl);
      }
    });
  }

}
