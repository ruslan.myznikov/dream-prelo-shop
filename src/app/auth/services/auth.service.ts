import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable()
export class AuthService {

  userData: BehaviorSubject<User> = new BehaviorSubject(new User('Anonim', 'mail', ''));

  currentUser: User;

  constructor(public afAuth: AngularFireAuth) {
    if(localStorage.getItem('name')) {
    this.currentUser = new User(localStorage.getItem('name'),
    localStorage.getItem('email'),
    localStorage.getItem('pictureUrl'));
    this.userData.next(this.currentUser);
    } else {
      this.currentUser = new User('Anonim', 'mail', 'http://pngimg.com/uploads/question_mark/question_mark_PNG54.png');
    }
   }

  /**
   * Do auth with Google
   * @returns {Promise<any>}
   */
  doGoogleLogin(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
        .signInWithPopup(provider)
        .then(res => {
          resolve(res);
        })
    })
  }

}
