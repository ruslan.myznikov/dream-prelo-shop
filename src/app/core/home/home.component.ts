import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { User } from '../../auth/models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  /**
   * User data
   * @type {User}
   */
  user: User = new User('Anonin', 'mail', '');

  /**
   * Contains subscription which react if user data chage
   * @type {Subscription}
   */
  userDataSubscription: Subscription;

  constructor(private authService: AuthService) {
    
    this.userDataSubscription = authService.userData.subscribe((data: User) => {
      this.user = new User(data.name, data.email, data.pictureUrl);
      
      console.log(this.user);
    });

   }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.userDataSubscription.unsubscribe();
  }

}
